
#include "pcbstatemachine.h"
#include <simutil.h>
#include <simvision2.h>
#include "database.h"
using namespace cv;
Q_LOGGING_CATEGORY( sm, "statemachine" )

PcbStateMachine::PcbStateMachine( QObject* parent ) : QObject( parent )
{
    initializeStateMachine();

    connect( m_pState00,      &QState::entered,              this, &PcbStateMachine::onS00StartEntered );
    connect( m_pState10,      &QState::entered,              this, &PcbStateMachine::onS10PrepareEntered );
    connect( m_pState20,      &QState::entered,              this, &PcbStateMachine::onS20LocationEntered );
    connect( m_pState30,      &QState::entered,              this, &PcbStateMachine::onS30DetectModeEntered );
    connect( m_pTransRetry00, &QSignalTransition::triggered, this, &PcbStateMachine::signalNextState );
}

void PcbStateMachine::initializeStateMachine()
{
    // initial state
    m_pStateMachine->setInitialState( m_pStateRoot );
    m_pStateRoot->setInitialState( m_pState00 );
    // transition next
    m_pState00->addTransition( this, &PcbStateMachine::signalNextState,  m_pState10 );
    m_pState10->addTransition( this, &PcbStateMachine::signalNextState,  m_pState20 );
    m_pState20->addTransition( this, &PcbStateMachine::signalNextState,  m_pState30 );
    m_pState30->addTransition( this, &PcbStateMachine::signalNextState,  m_pState00 );
    // transition retry
    m_pTransRetry00 = new QSignalTransition( this, &PcbStateMachine::signalRetryState, m_pState00 ); // m_pState00
    m_pStateRoot->addTransition( this, &PcbStateMachine::signalInitState, m_pState00 );
    //
    m_pStateMachine->start();
    qCInfo( sm ) << "[State Machine] Initialized and Started.";
}

void PcbStateMachine::onProcessImage( )
{
    data_list.hum_data();

    emit signalRetryState();
}

void PcbStateMachine::onS00StartEntered()
{
    qCDebug( sm ) << QString( "[S00] StartEntered. ==================================================" );
}

void PcbStateMachine::onS10PrepareEntered()
{


    emit signalNextState();
}

void PcbStateMachine::onS20LocationEntered()
{
    static RotatedRect sDetectedRotatedRect;
    static int siLastCenterPosition      = std::numeric_limits<int>::max();// INTMAX
    static double      sdLowPassPercent  = -1;
    qCDebug( sm ) << "[S20] LocationEntered";

   // PCBModel*           model = PCBModel::getCurrentModel();
    //PcbModelConfig&     ini   = model->getModelConfig();
    double dLocationPercent;
    int    iCurrentPosition, iMaxLength;
    bool   compact2_check = true;

    QMap<int, QVariant> paramMap;

}

void PcbStateMachine::onS30DetectModeEntered()
{
    qCInfo( sm ) << "[S30] InspectionEntered";

    emit signalProgressBar( 0 );

    m_bNewInspection = false;

    if ( m_rMainConfig.DetectionMode == 1 ) // manual(sensor)
    {
        emit signalNext1State();
    }
    else // 0: auto
    {
        emit signalNext2State();
    }
}

//void PcbStateMachine::onS31ManualDetectEntered()
//{
//    qCInfo( sm ) << "[S31] ManualDetectEntered";

//    emit signalNextState();

//}


void PcbStateMachine::onS90ResultEntered()
{
    qCInfo( sm ) << "[S90] ResultEntered";

    emit signalResult( m_mSourceImage );
    emit signalNextState();
    emit signalProgressBar( 3 );
}

