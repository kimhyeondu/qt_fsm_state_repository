#ifndef DATA_SELECT_H
#define DATA_SELECT_H
#include "database.h"
#include "imagesqltablemodel.h"

class data_select
{
public:
    ImageSqlTableModel *state_table = new ImageSqlTableModel(nullptr,database::getInstance()->init_database());
    data_select();
    ImageSqlTableModel temp_data();
    ImageSqlTableModel hum_data();
    ImageSqlTableModel rain_data();
    ImageSqlTableModel image_data();
    ~data_select();
};

#endif // DATA_SELECT_H
