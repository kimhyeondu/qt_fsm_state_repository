#ifndef STABLE_H
#define STABLE_H
// Add C includes here

#if defined __cplusplus
// Add C++ includes here
#include <opencv2/opencv.hpp>
#include <QtCore>
#include <QtWidgets>
#include <QtConcurrent>
#include <QtSql>

typedef QList<cv::Mat>      QMatList;
typedef QList<cv::UMat>     QUMatList;
typedef QMap<int, cv::Mat>  QIntMatMap;
typedef QMap<int, cv::UMat> QIntUMatMap;

#endif // __cplusplus
Q_DECLARE_METATYPE( cv::Mat )
Q_DECLARE_METATYPE( cv::UMat )
Q_DECLARE_METATYPE( cv::Point )
Q_DECLARE_METATYPE( cv::Point2f )
Q_DECLARE_METATYPE( cv::Rect )
Q_DECLARE_METATYPE( cv::RotatedRect )
#endif // STABLE_H
